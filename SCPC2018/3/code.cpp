/*


*/

#include <iostream>
#include <vector>
#include <memory.h>
#include <algorithm>

using namespace std;

typedef vector< vector<int> > Graph;


bool compare(const vector<int> &a, const vector<int> &b){
	return a.size() < b.size();
}

bool isConnected(Graph *graph, int a, int b){
	bool aa = false;
	bool bb = false;
	// search 'b' in graph[a].
//	cout << "[DEBUG] isConnected a,b : " << a << " " << b << endl;
	for (int i=0; i<(*graph)[a].size(); i++){
//	cout << "[DEBUG] isConnected (*graph)[a][i] : " << (*graph)[a][i] << endl;
		if ((*graph)[a][i] == b){
			aa = true;
			break;
		}
	}
	// search 'a' in graph[b].
	for (int i=0; i<(*graph)[b].size(); i++){
		if ((*graph)[b][i] == a){
			bb = true;
			break;
		}
	}
	
	return aa && bb;
}

void DEBUG(Graph graph){
	for (int i=1; i<graph.size(); i++){
		cout << "[DEBUG] " << i << "_Bucket: ";
		for (int j=0; j<graph[i].size(); j++){
			cout << graph[i][j] << " " ; 
		}
		cout << endl;
	}
}

void DEBUG(bool *arr, int N){
	cout << "DEBUG " ;
	for (int i=1; i<=N; i++){
		cout << arr[i] << " ";
	}
		cout << endl;
}
int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		Answer = 0;
		int N, M;
		cin >> N >> M;
		
		// Get input.
		Graph graph(N+1);
		int A, B;
		for (int i=0; i<M; i++){
			cin >> A >> B;
			graph[A].push_back(B);
			graph[B].push_back(A);
		} //DEBUG(graph);
		
		if (N > M){
			cout << "Case #" << test_case+1 << endl;
			cout << N << endl;
			continue;
		}

		// Initialize.
		bool checked[N+1];
		memset(checked, 0, sizeof(checked));

		// Traverse and remove.
		int a,b, removedCount=0;
		for (int i=1; i<=N; i++){
			if (checked[i])
				continue;
			if (graph[i].size() == 1){
				checked[i] = true;
				continue;
			}
			if (graph[i].size() == 2){
				a = graph[i][0];
				b = graph[i][1];
				if (isConnected(&graph, a, b)){
	//				cout << "[DEBUG] " << a << " and " << b << " is connected \n";
					checked[a] = true;
					checked[b] = true;
					removedCount++;
				}
			checked[i] = true;
			}
		}
//		DEBUG(checked, N);

		// More than 3
		int checkNum = 3;
		while (checkNum < 100){

				//	cout << "[DEBUG] checkNum " << checkNum << endl;
			for (int i=1; i<=N; i++){
				if (checked[i])
					continue;

				int size = graph[i].size();
				int count = 0;
				for (int x=0; x<graph[i].size(); x++){
					for (int y=x+1; y<graph[i].size(); y++){
						if (isConnected(&graph, graph[i][x], graph[i][y])){
							count++;
					//cout << "[DEBUG] " << graph[i][x] << " and " << graph[i][y] << " is connected \n";
					//		cout << "[DEBUG] count " <<  count << endl;
						}
					}
				} 
				if (count >= size - 1){
					removedCount++;
	//	DEBUG(checked, N);
					for (int x=0; x<graph[i].size(); x++){
						checked[graph[i][x]] = true;
					}
	//	DEBUG(checked, N);
				} // if
				checked[i] = true;
			}// for
		checkNum++;
		}// while

		Answer = N - removedCount;

		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;
}
