/*


*/

#include <iostream>
#include <list>
#include <queue>

using namespace std;

typedef list<int> Student; 

int Answer;

bool compare(int a, int b){
	return a < b;
}

void DEBUG(Student list){
	if (list.size() <= 0){
		cout << "List empty " << endl;
			return ;
	}
	cout << "Size : " << list.size() << endl;
	for (Student::iterator IterPos = list.begin();
			IterPos != list.end(); ++IterPos){
		cout << *IterPos << " " ;
	}
	cout << endl;
}
int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		int N, K;
		Answer = 0;
		cin >> N >> K;

		// INPUT 
		int input;
		priority_queue<int, vector<int>, greater<int> > students;
		priority_queue<int, vector<int>, greater<int> > minimaxQueue;
		for(int i=0; i<N; i++){
			cin >> input;
			students.push(input);
		}

		//cout << "[DEBUG list size : " << students.size() << endl;

		int a;
		int minBoundary;
		while(!students.empty()){
			//assert(students.empty() == False);
			a = students.top();
			students.pop();
			
			// Initial one bus. 
			if (minimaxQueue.empty()){
				minimaxQueue.push(a);
			}
			// Check whether 'a' is safe or not.
			else{
				minBoundary = minimaxQueue.top();
				//cout << "[DEBUG] minBoundary : " << minBoundary << endl;
				if (minBoundary + K < a){
					minimaxQueue.pop();
					minimaxQueue.push(a);
					continue; // Do not need to add a bus more. 
				}
				else { // If a should not be together,, add one more bus. 
					minimaxQueue.push(a);
				}
			}
			//cout << "[DEBUG] queue size : " << minimaxQueue.size() << endl;
			//cout << "[DEBUG] list size : " << students.size() << endl;
		}

			Answer = minimaxQueue.size();

		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;
}
