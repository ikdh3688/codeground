



data = []
def readFile():
    with open('./output', 'r') as f:
        while True:
            line = f.readline()[:-1] 
            if not line:
                break
            if "Case" in line:
                continue
            line = line.split(' ')
            for i in line[1:]:
                data.append(i)

def check(word):
    length = len(word)
    for i in range(length/2+1):
        if word[i] != word[length - i - 1]:
            return False
    return True
        



readFile()


ret = True
del data[0]
for i in data:
    if not check(i):
        ret = False
print(ret)
