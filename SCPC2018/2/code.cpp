/*


*/

#include <iostream>
#include <vector>
#include <string>
#include <memory.h>
#include <algorithm>
#include <set>
using namespace std;


int LIMIT=10000;

int Answer;
string toCstr (int x){
	string s = to_string(x);
	return s;
}

bool isPalindrome (string str){
	int length = str.length();
	for (int i=0; i<length/2; i++){
		if (str.at(i) != str.at(length - i -1)) {
			return false;
		}
	}
	return true;
}
void findPalindrome (vector<int> *huimen, bool *oneBool){
	for (int i=1; i<=LIMIT; i++){
		string str = toCstr(i);
		if (isPalindrome(str)){
			huimen->push_back(i);
			oneBool[i] = true;
			//cout << "Insert "<<i << endl;
		}
	}

}

void DEBUG(vector<int> a){
	for (vector<int>::iterator IterPos = a.begin();  IterPos != a.end(); ++IterPos){
		cout << *IterPos << " " ;
	}
	cout << endl;
}

void DEBUG(set<int> a){
	for (set<int>::iterator IterPos = a.begin();  IterPos != a.end(); ++IterPos){
		cout << *IterPos << " " ;
	}
	cout << endl;
}

void DEBUG(bool* a){
	int count = 0;
	for (int i=1; i<=LIMIT; i++){
		cout << a[i] << " ";
		if (a[i]){
			count++;
		}
	}
	cout << "total : " << count << endl;
}
void DEBUG(int* a){
	for (int i=1; i<=LIMIT; i++){
		cout << a[i] << " ";
	}
	cout << endl;
}

void DEBUG( int* b, int* c){
	for (int i=1; i<=LIMIT; i++){
		cout << "[" << i << "] "  << b[i] << "+" << c[i]  << endl;
	}
	cout << endl;

}

int main(int argc, char** argv)
{
	int T, test_case;
	bool oneBool[LIMIT+1]; // One boolean for a huimen.
	bool twoSumBool[LIMIT+1]; // Possible combinations with two huimens.
	int twoSumBool1[LIMIT+1]; // Store huimen value 1.
	int twoSumBool2[LIMIT+1]; // Store huimen value 2.

	// Initialize
	memset(oneBool, 0, sizeof(oneBool));
	memset(twoSumBool, false, sizeof(twoSumBool));
	memset(twoSumBool1, 0, sizeof(twoSumBool1));
	memset(twoSumBool2, 0, sizeof(twoSumBool2));

	// Find all palindrome within 10,000.
	vector<int> huimen;
	findPalindrome(&huimen, oneBool);
	int size = huimen.size();
		
	// Find all combinations with 'two' huimens.
	int sum = 0;
	for (int i=0; i<size; i++){
		for (int j=i; j<size; j++){
			sum = huimen[i]+huimen[j];
			if (sum <= LIMIT){
				// Skip overlapped combination.
				if (!twoSumBool[sum]){
					twoSumBool[sum] = true;
	 				twoSumBool1[sum] = huimen[i];
 					twoSumBool2[sum] = huimen[j];
				}
			} 
		} //for
	} //for

	//DEBUG(huimen);
	//DEBUG(oneBool);
	//DEBUG(twoSum);
	//DEBUG(twoSumBool);
	//DEBUG(twoSumBool1);
	//DEBUG(twoSumBool2);
	//DEBUG(twoSumBool1,twoSumBool2);

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		int n;
		Answer = 0;
		cin >> n;
		if (n == 0){
			cout << "Case #" << test_case+1 << endl;
			cout << "1 " << n << endl;
			continue;
		}
		vector<int> answer;
		
		int lastValue = 0;
		cout << "Case #" << test_case+1 << endl;
		// Check whether it is huimen as it is.
		if (oneBool[n]){
			cout << "1 " << n << endl;
		}
		// Check whether it can be represented with two huimens.
		else if (twoSumBool[n]){
			answer.push_back(twoSumBool1[n]);
			answer.push_back(twoSumBool2[n]);
			sort(answer.begin(), answer.end());
			cout << "2 " << answer[1] << " " << answer[0] << endl;
		}
		// Three and else.
		else{
			int value = 0;
			int huimen1,huimen2;
			for(int i=0; i<n; i++){
				// 
				if (twoSumBool[i]) {
					value = n - i;
					if(oneBool[value]){
						lastValue = value;
						huimen1 = twoSumBool1[i];
						huimen2 = twoSumBool2[i];
						break;
					}
				}
			}
			// 
			if (lastValue != 0){
	//			cout << "lastValue " << lastValue << endl;
				answer.push_back(huimen1);
				answer.push_back(huimen2);
				answer.push_back(lastValue);
				sort(answer.begin(), answer.end());
				cout << "3 " << answer[2] << " " << answer[1] << " " << answer[0] << endl;
				//cout << "KDH " <<n << " " << answer[2] << " " << answer[1] << " " << answer[0] << endl;
			}
			else 
			cout << "0 " << endl;
		}

	}

	return 0;
}
