/*


*/

#include <iostream>
#include <vector>
#include <set>
using namespace std;

typedef long long int int64;
typedef vector<int64> Seq;
typedef set<int64> Set;

int64 gcd(int64 a, int64 b){
  return b ? gcd(b, a%b) : a;
}

int64 smallNumbers(int64 number){
   int ret = 0;
   int64 i =1;

   if (number <= 1) 
	return 1;
   for(i=1; i*i<number; ++i){
	if (number % i == 0)
	   ret += 2;
   }
   if ( i*i == number) 
	ret++;
   return ret;

}

void DEBUG(Seq numbers, Set diff){
  for (int i=0; i<numbers.size(); i++){
    cout << numbers[i] << " " ;
  }
  cout << endl;
  for (set<int64>::iterator IterPos = diff.begin();  IterPos != diff.end(); ++IterPos){
    cout << *IterPos << " " ;
  }
  cout << endl;
}

int Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{

		Answer = 0;
		/*TODO*/
		int N;
		int64 x;
		cin >> N;
		Seq numbers(N);
		Set diff;

		// Get input
		cin >> x;
		numbers[0] = x; 
		for (int i=1; i<N; i++){
			cin >> x;
			numbers[i] = x;
			diff.insert(x-numbers[i-1]);
		}
		//DEBUG(numbers,diff);
		Seq gcds(diff.size());
		set<int64>::iterator IterPos = diff.begin();
		for (int i=0; i<diff.size(); i++){
			gcds[i] = *IterPos;
			IterPos++;
		}

		if (diff.size() != 1 && *diff.begin() == 0) {
			//cout << "diff.size() != 1 && *diff.begin() == 0"<< endl;
			//DEBUG(numbers,diff);
			Answer = 0;
		}else if (diff.size() == 1){
			//cout << "diff.size() == 1 "<< endl;
			//DEBUG(numbers,diff);
			Answer = smallNumbers(gcds[0]);
		}else {
			//cout << "else " << endl ; 
			//DEBUG(numbers,diff);
   		   for (int i=1; i<diff.size(); i++)
		      gcds[i] = gcd(gcds[i], gcds[i-1]);
		   Answer = smallNumbers( gcds[diff.size()-1] );
		}
		

		cout << "Case #" << test_case+1 << endl;
		cout << Answer << endl;
	}

	return 0;
}
