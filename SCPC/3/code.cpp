/*
 * =====================================================================================
 *
 *       Filename:  code.cpp
 *
 *    Description:  Finding battery location.
 *  
 *        Version:  1.0
 *        Created:  04/28/18 
 *       Revision:  none
 *       Compiler:  gcc
 *        
 *         Author:  Do Hyung Kim 
 *   Organization:  
 * 
 * =====================================================================================
 */
#include <iostream>
#include <vector>

using namespace std;

struct Line{
   unsigned a;
   unsigned b;
};


unsigned MIN(unsigned x, unsigned y){
   return x>y? y : x;
}

unsigned MAX(unsigned x, unsigned y){
   return x>y? x : y;
}

unsigned dist(unsigned x, unsigned y){
   return x>y? x-y : y-x;
}


/* Find out the optimal battery point from an initial location x. */
unsigned solve(unsigned x, vector<Line> lines){

	unsigned temp = 0;
	int N = lines.size();
	
	while (true){
		unsigned minimum = 1e10;
		unsigned maximum = 0;
		for(int i=0; i<N; i++){
			if (dist(x, lines[i].a) < dist(x, lines[i].b)) {
				minimum = MIN(minimum, lines[i].a);
				maximum = MAX(maximum, lines[i].a);
			}else{
				minimum = MIN(minimum, lines[i].b);
				maximum = MAX(maximum, lines[i].b);
			}
			cout << "[KDH] min : " <<  minimum << " max :" <<  maximum << endl;
		}
		temp = x;
		x = (minimum+maximum)/2;
		if (x == temp)
			return x;
	
	}
}

unsigned max_dist(unsigned x, vector<Line> lines){
	unsigned max_val;
	for(int i = 0; i < lines.size(); i++) {
		max_val = MAX(MIN(dist(x, lines[i].a), dist(x, lines[i].b)), max_val);
	}
	return max_val;
}

double Answer;

int main(int argc, char** argv)
{
	int T, test_case;

	cin >> T;
	for(test_case = 0; test_case  < T; test_case++)
	{
		int N;
		Answer = 0;
		cin >> N;
		vector<Line> lines_x(N);
		vector<Line> lines_y(N);

		// get liness
		unsigned inputs[4];
		for (int i=0; i<N; i++){
			cin >> inputs[0] >> inputs[2] >> inputs[1] >> inputs[3];
			//Swap(inputs[0], inputs[1]);
			//Swap(inputs[2], inputs[3]);
			lines_x[i].a = inputs[0]*2;
			lines_x[i].b = inputs[1]*2;
			lines_y[i].a = inputs[2]*2;
			lines_y[i].b = inputs[3]*2;
			//cout << lines_x[i].a << " " <<  lines_x[i].b<< " " << lines_y[i].a << " " <<  lines_y[i].b  << endl;
		}

		/* Check the answer from the left most battery point.*/
		/* Check the answer from the right most battery point.*/
		double AnswerX = solve(0, lines_x); 
		double AnswerY = solve(0, lines_y); 
		double AnswerX2 = solve(1e10, lines_x);  
		double AnswerY2 = solve(1e10, lines_y); 


		cout << "[KDH] 1: " << AnswerX <<  " " << AnswerY << endl;
		cout << "[KDH] 2: " << AnswerX2 <<  " " << AnswerY2 << endl;

		/* Get max Chebyshev distance based on optimal battery point. */
		unsigned sol_dist[4] = {0,0,0,0};
		for(int i = 0; i < N; i++) {
			sol_dist[0] = MAX(MIN(dist(AnswerX, lines_x[i].a), dist(AnswerX, lines_x[i].b)), sol_dist[0]);
			sol_dist[1] = MAX(MIN(dist(AnswerY, lines_y[i].a), dist(AnswerY, lines_y[i].b)), sol_dist[1]);
			sol_dist[2] = MAX(MIN(dist(AnswerX2, lines_x[i].a), dist(AnswerX2, lines_x[i].b)), sol_dist[2]);
			sol_dist[3] = MAX(MIN(dist(AnswerY2, lines_y[i].a), dist(AnswerY2, lines_y[i].b)), sol_dist[3]);
		}
		cout << "[KDH] 12,8 " << max_dist(12, lines_x) << " " << max_dist(8,lines_y) << endl;
		cout << "[KDH] 16,12 " << max_dist(16, lines_x) << " " << max_dist(12,lines_y) << endl;
		for (int i=0; i<4; i++){
			cout << "[KDH] sol_dist[" << i << "] :" << sol_dist[i]  << endl;
		}
		/* sol_dist[0] will be the answer.  */
		sol_dist[0] = sol_dist[0]>sol_dist[1] ? sol_dist[0] : sol_dist[1] ;
		sol_dist[2] = sol_dist[2]>sol_dist[3] ? sol_dist[2] : sol_dist[3] ;
		sol_dist[0] = sol_dist[0]<sol_dist[2] ? sol_dist[0] : sol_dist[2] ;

		cout << "Case #" << test_case+1 << endl;

		Answer = sol_dist[0]; Answer /= 2;	
		if (Answer != (int)Answer) {
			cout << (int)Answer << ".5" << endl;
		}else {
			cout << Answer << endl;
		}
	}

	return 0;
}
